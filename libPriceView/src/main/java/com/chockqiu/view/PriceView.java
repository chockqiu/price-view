package com.chockqiu.view;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.chockqiu.view.priceview.R;

public class PriceView extends FrameLayout {

    public PriceView(Context context) {
        this(context, null);
    }

    public PriceView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PriceView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.PriceView);
            unitText = a.getString(R.styleable.PriceView_pv_unitText);
            priceText = a.getString(R.styleable.PriceView_pv_priceText);
            price2Text = a.getString(R.styleable.PriceView_pv_price2Text);
            delLineText = a.getString(R.styleable.PriceView_pv_delLineText);

            priceTextColor = a.getColor(R.styleable.PriceView_pv_priceTextColor, priceTextColor);
            price2TextColor = a.getColor(R.styleable.PriceView_pv_price2TextColor, price2TextColor);
            unitTextColor = a.getColor(R.styleable.PriceView_pv_unitTextColor, unitTextColor);
            delLineTextColor =
                    a.getColor(R.styleable.PriceView_pv_delLineTextColor, delLineTextColor);

            priceTextSize =
                    a.getDimensionPixelSize(R.styleable.PriceView_pv_priceTextSize, (int) priceTextSize);
            price2TextSize =
                    a.getDimensionPixelSize(R.styleable.PriceView_pv_price2TextSize, (int) price2TextSize);
            unitTextSize =
                    a.getDimensionPixelSize(R.styleable.PriceView_pv_unitTextSize, (int) unitTextSize);
            delLineTextSize =
                    a.getDimensionPixelSize(R.styleable.PriceView_pv_delLineTextSize, (int) delLineTextSize);

            priceTextLeftPadding = a.getDimensionPixelSize(
                    R.styleable.PriceView_pv_priceTextLeftPadding,
                    priceTextLeftPadding
            );
            price2TextLeftPadding = a.getDimensionPixelSize(
                    R.styleable.PriceView_pv_price2TextLeftPadding,
                    price2TextLeftPadding
            );
            unitTextLeftPadding = a.getDimensionPixelSize(
                    R.styleable.PriceView_pv_unitTextLeftPadding,
                    unitTextLeftPadding
            );
            delLineTextLeftPadding = a.getDimensionPixelSize(
                    R.styleable.PriceView_pv_delLineTextLeftPadding,
                    delLineTextLeftPadding
            );

            unitImg = a.getResourceId(R.styleable.PriceView_pv_unitImg, unitImg);
            unitImgWidth = a.getDimensionPixelSize(
                    R.styleable.PriceView_pv_unitImgWidth,
                    unitImgWidth
            );
            unitImgHeight = a.getDimensionPixelSize(
                    R.styleable.PriceView_pv_unitImgHeight,
                    unitImgHeight
            );
            showMode = a.getInt(R.styleable.PriceView_pv_showMode, showMode);
            a.recycle();
        }
        initView();
        updateView();
    }

    private int unitTextColor = Color.BLACK;
    private int priceTextColor = Color.BLACK;
    private int price2TextColor = Color.BLACK;
    private int delLineTextColor = Color.BLACK;

    private float unitTextSize = 16.f;

    private float priceTextSize = 16f;

    private float price2TextSize = 16f;

    private float delLineTextSize = 16f;

    private String unitText = null;
    private String priceText = null;
    private String price2Text = null;
    private String delLineText = null;

    private int unitTextLeftPadding = 0;
    private int priceTextLeftPadding = 0;
    private int price2TextLeftPadding = 0;
    private int delLineTextLeftPadding = 0;

    private int unitImg = -1;
    private int unitImgWidth = ViewGroup.LayoutParams.WRAP_CONTENT;
    private int unitImgHeight = ViewGroup.LayoutParams.WRAP_CONTENT;

    private int showMode = 0;
    private View mainView = null;

    public ImageView ivUnit = null;
    public TextView tvUnit = null;
    public TextView tvPrice = null;
    public TextView tvPrice2 = null;
    public TextView tvUnderLine = null;

    private void initView() {
        mainView = LayoutInflater.from(getContext()).inflate(R.layout.layout_price_view, this);
        ivUnit = mainView.findViewById(R.id.iv_unit);
        tvUnit = mainView.findViewById(R.id.tv_unit);
        tvPrice = mainView.findViewById(R.id.tv_price);
        tvPrice2 = mainView.findViewById(R.id.tv_price2);
        tvUnderLine = mainView.findViewById(R.id.tv_underline);
        tvUnderLine.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
    }


    private void updateView() {
        ivUnit.getLayoutParams().width = unitImgWidth;
        ivUnit.getLayoutParams().height = unitImgHeight;
        ivUnit.requestLayout();
        if (unitImg > 0) {
            ivUnit.setImageResource(unitImg);
        }
        tvUnit.setTextColor(unitTextColor);
        tvUnit.setTextSize(px2dp(unitTextSize));
        tvUnit.setText(unitText);
        tvUnit.setPadding(unitTextLeftPadding, 0, 0, 0);

        tvPrice.setTextColor(priceTextColor);
        tvPrice.setTextSize(px2dp(priceTextSize));
        tvPrice.setText(priceText);
        tvPrice.setPadding(priceTextLeftPadding, 0, 0, 0);

        tvPrice2.setTextColor(price2TextColor);
        tvPrice2.setTextSize(px2dp(price2TextSize));
        tvPrice2.setText(price2Text);
        tvPrice2.setPadding(price2TextLeftPadding, 0, 0, 0);

        tvUnderLine.setTextColor(delLineTextColor);
        tvUnderLine.setTextSize(px2dp(delLineTextSize));
        tvUnderLine.setText(delLineText);
        tvUnderLine.setPadding(delLineTextLeftPadding, 0, 0, 0);
    }

    public float px2dp(float f) {
        float scale = Resources.getSystem().getDisplayMetrics().density;
        return (f / scale + 0.5f);
    }


    public int dp2px(float f) {
        float scale = Resources.getSystem().getDisplayMetrics().density;
        return (int) (f * scale + 0.5f);
    }
}