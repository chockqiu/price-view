# PriceView

#### 介绍
Android显示价格的自定义View

先看下具体效果

![具体效果.png](img/13651212-0409d2343932ef43.png)

#### 使用方式

在工程根目录的**build.gradle**中添加
```
allprojects {
	repositories {
		maven { url 'https://www.jitpack.io' }
		...
	}
}
```
添加依赖
```
implementation 'com.gitee.tinyqiu:price-view:1.5'
```

在xml中添加布局
```
<com.chockqiu.view.PriceView
    android:layout_width="wrap_content"
    android:layout_height="30dp"
    app:pv_delLineText="2000"
    app:pv_delLineTextColor="#cccccc"
    app:pv_delLineTextLeftPadding="3dp"
    app:pv_delLineTextSize="12sp"
    app:pv_price2Text=".99"
    app:pv_price2TextColor="#333333"
    app:pv_price2TextSize="10sp"
    app:pv_priceText="1899"
    app:pv_priceTextColor="#333333"
    app:pv_priceTextLeftPadding="1dp"
    app:pv_priceTextSize="16sp"
    app:pv_unitText="¥"
    app:pv_unitTextColor="#333333"
    app:pv_unitTextSize="10sp" />
```

#### 使用说明

整体可分为5个部分，分别是[单位][icon][价格][小数点数][划线价]

其中[单位]跟[icon]一般不会同时显示, 所以组合有大致有如下两种

[icon][价格][小数点数][划线价]

[单位][价格][小数点数][划线价]

###### 设置规则
- 每个部位不设置值便隐藏，且不占用空间；
- 文字部分可设置字体大小、颜色；
- 文字部分可设置LeftPadding，控制与左边部分的间距；
- 文字部分可设置字体；
- icon部分可设置宽高。

博客地址: https://www.jianshu.com/p/ea3360103682

#### 如果对你有帮助就点个赞支持下吧~~~