package com.chockqiu.demo.priceviewdemo;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.chockqiu.view.PriceView;

/**
 * @author Administrator
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ViewGroup ly = findViewById(R.id.ly_din);
        int c = ly.getChildCount();
        for (int i = 0; i < c; i++) {
            View v = ly.getChildAt(i);
            if (v instanceof TextView) {
                setDinATextView((TextView) v);
            } else if (v instanceof PriceView) {
                setDinATextView((PriceView) v);
            }
        }
    }

    void setDinATextView(PriceView mPriceView) {
        Typeface tf = Typeface.createFromAsset(
                mPriceView.getContext().getAssets(),
                "DIN_Alternate_Bold.ttf"
        );
        mPriceView.tvUnit.setTypeface(tf);
        mPriceView.tvPrice.setTypeface(tf);
        mPriceView.tvUnderLine.setTypeface(tf);
    }

    void setDinATextView(TextView textView) {
        Typeface tf = Typeface.createFromAsset(
                textView.getContext().getAssets(),
                "DIN_Alternate_Bold.ttf"
        );
        textView.setTypeface(tf);
    }
}
